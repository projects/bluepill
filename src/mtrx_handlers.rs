//! Central matrix handling crate
//! 
//! This crate is the central point for matrix functionality

use matrix_sdk::{
    ruma::events::room::message::SyncRoomMessageEvent,
    Client,
};

fn theprintf(thev: SyncRoomMessageEvent) {
    println!("Received a message {:?}", thev);
}

/// Global event handler register.
///
/// This is the central point which registers all used
/// Event handlers:
/// - SyncRooomMessageEvent

pub fn add_event_handlers(client: &Client) {
    client.add_event_handler(|ev: SyncRoomMessageEvent| async move {
	theprintf(ev);
    });
}
