use matrix_sdk::{
    config::SyncSettings,
    Client,
    AuthSession,
};

use bluepill::mtrx_handlers::add_event_handlers;

use clap::Parser;

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    /// The username to log in
    #[arg(short, long)]
    username: String,

    /// Hostname of the Matrix instance <https://example.com>
    #[arg(long,short('n'))]
    hostname: String,

    /// The password to log in
    #[arg(short, long)]
    password: String,
}

#[tokio::main(flavor = "current_thread")]
async fn main() -> anyhow::Result<()> {
    let args = Args::parse();
    
    let matrixloop = matrix_loop(
	&args.username,
	&args.hostname,
	&args.password,
    ).await;

    matrixloop
}

async fn matrix_loop(theuser: &String,
		     hostname: &str,
		     thepassword: &str,
) -> anyhow::Result<()> {

    let client = Client::builder().
	homeserver_url(hostname).build().await?;

    // old session check

    let auth = client.matrix_auth();

    if auth.refresh_token().is_some()
	&& auth.refresh_access_token().await.is_ok()
    {
	println!("get old session");
	persist_session(client.session());
    }
    else
    {
	// we need to login
	
	let response = client.matrix_auth().
	    login_username(theuser, thepassword).send().await?;
	
	// Persist the `MatrixSession` so it can later be used to restore the login.

	auth.restore_session((&response).into()).await?;
    }

    add_event_handlers(&client);

    // Syncing is important to synchronize the client with the server
    // This method will never return unless there is an error.
    
    client.sync(SyncSettings::default()).await?;
    
    Ok(())
}

fn persist_session(_session: Option<AuthSession>) {

    // persisting stuff
}
